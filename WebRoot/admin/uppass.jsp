<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head> 
<link rel="stylesheet" href="/travel/admin/css/common.css" type="text/css" />
<title>管理区域</title>
<style type="text/css">
<!--
.STYLE5 {color: orange}

.mytab {
	font-size: 12px;
	width:99.1%;
	line-height: 150%;
	text-decoration: none;
	word-wrap: break-word;
	border:#DBE6E3 solid 1px; border-bottom:0; border-right:0;
	line-height:175%; 
	margin:5px -9px;
	margin-left: -12px;	
}
.mytab td{ padding:2px 5px; border:#DBE6E3 solid 1px; border-left:0; border-top:0;}

-->
    </style>
</head>

<body>
<form action="/travel/travel?ac=uppass" name="f1" method="post">
 

  <table width="99%" border="0" align="center"  cellpadding="3" cellspacing="1" class="mytab">
  
<tr align="center" style="">
          <td colspan="2"  background="/travel/admin/images/bg.gif"
				bgcolor="#FFFFFF" height="24" class="STYLE3"><strong>修改密码</strong></td>
        </tr>
  
    <tr>
      <td width="18%" height="24" align="center" class=""> 请输入原密码 </td>
      <td width="82%" height="24"><input name="olduserpass" type="password" id="olduserpass" /></td>
    </tr>
    <tr>
      <td height="24" align="center" class=""> 请输入新密码 </td>
      <td height="24"> 
        <input name="userpass" type="password" id="userpass" />       </td>
    </tr>
    <tr>
      <td height="24" align="center" class=""> 请重复新密码 </td>
      <td height="24"> 
        <input  name="copyuserpass" type="password" id="copyuserpass" />      </td>
    </tr>
    <tr>
      <td height="25" colspan="2" align="center"  > 
        <input type="submit" name="Submit" onmousedown="check();" value="提交信息" />
        &nbsp;&nbsp;&nbsp;
        <input type="reset" name="Submit" value="重新填写" />
      </td>
    </tr>
  </table>
 
</form>
</body>
</html>
<script type="text/javascript">
function check()
{
if(f1.olduserpass.value=="")
{
alert("请输入原密码");
return;
}
if(f1.userpass.value=="")
{
alert("请输入新密码");
return;
}
if(f1.copyuserpass.value!=f1.userpass.value)
{
alert("两次密码输入不一致");
return;
}
}
</script>
<script type="text/javascript">
<%
String error = (String)request.getAttribute("error");
if(error!=null)
{
 %>
 alert("原密码不对");
 <%}%>
 <%
String suc = (String)request.getAttribute("suc");
if(suc!=null)
{
 %>
 alert("操作成功");
 <%}%>
 </script>
