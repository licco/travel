// 导航栏配置文件
var outlookbar=new outlook();
var t;
t=outlookbar.addtitle('基本设置','基本设置',1)
outlookbar.additem('查看个人资料',t,'ckpinfo.jsp')
outlookbar.additem('修改个人资料',t,'psysuserxg.jsp')
outlookbar.additem('更改登录密码',t,'uppass.jsp')

t=outlookbar.addtitle('系统管理','系统管理',1)
outlookbar.additem('添加管理员',t,'addsysusertj.jsp')
outlookbar.additem('管理员查看',t,'sysusercx.jsp')
outlookbar.additem('会员管理',t,'memberscx.jsp')

t=outlookbar.addtitle('信息管理','信息管理',1)
outlookbar.additem('旅游资讯管理',t,'lvyzixcx.jsp')
outlookbar.additem('景点管理',t,'jdianglcx.jsp')
outlookbar.additem('景点路线管理',t,'jdluxiancx.jsp')
outlookbar.additem('热门消费地点',t,'rmenxfddcx.jsp')
outlookbar.additem('广告图片',t,'guanggao.jsp')
outlookbar.additem('杭州旅游',t,'syny.jsp')
outlookbar.additem('网站公告',t,'wzgg.jsp')
outlookbar.additem('联系我们',t,'lxwm.jsp')
outlookbar.additem('留言管理',t,'lybcx.jsp')
outlookbar.additem('友情链接',t,'yqlj.jsp')
